#!/bin/env python3

import os
import glob
from guy import Guy
import vbuild

class Bibcast(Guy):
    """
    # Websocket Backend for Bibcast

    Renders the Vue components
    """
    template = "src/template.html"

    def render(self, includeGuyJs=False):
        vue = vbuild.render("src/*.vue")
        os.chdir("static")
        preload = 'preload = ' + str(glob.glob("*.mp3")
                                   + glob.glob("*.ogg")) + '\n\n'
        os.chdir("..")
        template = open(Bibcast.template).read()
        template = template.replace("/* SCRIPT */", preload + str(vue.script))
        template = template.replace("/* STYLE */", str(vue.style))
        return template.replace("<!-- HTML -->", str(vue.html))

if __name__ == "__main__":
    PORT = os.environ.get('PORT') or 7777
    Bibcast().serve(port=int(PORT), open=False)
